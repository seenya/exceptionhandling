﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionServices
{
    public class ExceptionPerformanceService
    {
        public bool[] RunLoopWithExceptions(int iterations)
        {
            var results = new bool[iterations];

            for (var i = 0; i < iterations; i++)
            {
                try
                {
                    results[i] = ThrowExceptionOnFailure(1);
                }
                catch (Exception e)
                {
                    results[i] = false;
                }
            }

            return results;
        }


        public bool[] RunLoopWithoutExceptions(int iterations)
        {
            var results = new bool[iterations];

            for (var i = 0; i < iterations; i++)
            {
                try
                {
                    results[i] = ReturnFalseOnFailure(1);
                }
                catch (Exception e)
                {
                    results[i] = false;
                }
            }

            return results;
        }

        private bool ThrowExceptionOnFailure(int testValue)
        {
            if (testValue <= 2)
            {
                throw new ApplicationException("Your value isn't greater than 2!");
            }

            return true;
        }

        private bool ReturnFalseOnFailure(int testValue)
        {
            if (testValue <= 2)
            {
                return false;
            }

            return true;
        }
    }
}
