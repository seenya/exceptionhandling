﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionServices
{
    public class CallingService
    {
        public void DoWork()
        {
            try
            {
                var service = new ExceptionThrowingService();
                service.ErroneousMethod();
            }
            catch (Exception e)
            {
                // Perform special handling, logging, etc.
                throw e; // Line 21!
            }
        }

        public void DoWorkV2()
        {
            try
            {
                var service = new ExceptionThrowingService();
                service.ErroneousMethod();
            }
            catch (Exception ex)
            {
                // Perform special handling, logging, etc.
                throw;
            }
        }
    }
}
