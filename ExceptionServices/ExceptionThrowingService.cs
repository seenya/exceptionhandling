﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionServices
{
    public class ExceptionThrowingService
    {
        public void DoWork()
        {
            try
            {
                ErroneousMethod();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void DoWorkV2()
        {
            try
            {
                ErroneousMethod();
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public void ErroneousMethod()
        {
            throw new Exception("I wasn't able to complete that request!");
        }
    }
}
