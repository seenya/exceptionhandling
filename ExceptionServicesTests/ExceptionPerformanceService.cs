﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExceptionServices;
using NUnit.Framework;

namespace ExceptionServicesTests
{
    [TestFixture()]
    public class ExceptionPerformanceServiceTests
    {
        [Test()]
        public void RunLoopWithAndWithoutExceptions_CompareTimings()
        {
            var service = new ExceptionPerformanceService();

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            service.RunLoopWithExceptions(100);
            stopwatch.Stop();
            Console.WriteLine($"100 iterations with exceptions took {stopwatch.ElapsedMilliseconds}ms to execute.");

            stopwatch.Reset();
            stopwatch.Start();
            service.RunLoopWithoutExceptions(100);
            stopwatch.Stop();
            Console.WriteLine($"100 iterations without exceptions took {stopwatch.ElapsedMilliseconds}ms to execute.");

            stopwatch.Reset();
            stopwatch.Start();
            service.RunLoopWithExceptions(1000);
            stopwatch.Stop();
            Console.WriteLine($"1,000 iterations with exceptions took {stopwatch.ElapsedMilliseconds}ms to execute.");

            stopwatch.Reset();
            stopwatch.Start();
            service.RunLoopWithoutExceptions(1000);
            stopwatch.Stop();
            Console.WriteLine($"1,000 iterations without exceptions took {stopwatch.ElapsedMilliseconds}ms to execute.");

            stopwatch.Reset();
            stopwatch.Start();
            service.RunLoopWithExceptions(10000);
            stopwatch.Stop();
            Console.WriteLine($"10,000 iterations with exceptions took {stopwatch.ElapsedMilliseconds}ms to execute.");

            stopwatch.Reset();
            stopwatch.Start();
            service.RunLoopWithoutExceptions(10000);
            stopwatch.Stop();
            Console.WriteLine($"1,0000 iterations without exceptions took {stopwatch.ElapsedMilliseconds}ms to execute.");

            stopwatch.Reset();
            stopwatch.Start();
            service.RunLoopWithExceptions(100000);
            stopwatch.Stop();
            Console.WriteLine($"100,000 iterations with exceptions took {stopwatch.ElapsedMilliseconds}ms to execute.");

            stopwatch.Reset();
            stopwatch.Start();
            service.RunLoopWithoutExceptions(100000);
            stopwatch.Stop();
            Console.WriteLine($"100,000 iterations without exceptions took {stopwatch.ElapsedMilliseconds}ms to execute.");

            // This takes roughly 40 seconds, if the audience wants this as evidence, then feel free to uncomment
            stopwatch.Reset();
            stopwatch.Start();
            service.RunLoopWithExceptions(1000000);
            stopwatch.Stop();
            Console.WriteLine($"1,000,000 iterations with exceptions took {stopwatch.ElapsedMilliseconds}ms to execute.");

            stopwatch.Reset();
            stopwatch.Start();
            service.RunLoopWithoutExceptions(1000000);
            stopwatch.Stop();
            Console.WriteLine($"1,000,000 iterations without exceptions took {stopwatch.ElapsedMilliseconds}ms to execute.");

        }
    }
}
