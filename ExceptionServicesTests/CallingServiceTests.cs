﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExceptionServices;
using NUnit.Framework;

namespace ExceptionServicesTests
{
    [TestFixture()]
    public class CallingServiceTests
    {
        [Test()]
        public void ThrowEx_StrackTraceIsRewrittenAndAppearsAsCallingService()
        {
            var service = new CallingService();

            try
            {
                service.DoWork();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        [Test()]
        public void ThrowWithoutEx_StrackTraceIsAtLineOfExceptionInActualOffendingService()
        {
            var service = new CallingService();

            try
            {
                service.DoWorkV2();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
