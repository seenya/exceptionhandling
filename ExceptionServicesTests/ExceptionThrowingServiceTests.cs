﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using ExceptionServices;

namespace ExceptionServicesTests
{
    [TestFixture()]
    public class ExceptionThrowingServiceTests
    {

        // Notice how in this test
        [Test()]
        public void ThrowEx_StrackTraceIsRewritten()
        {
            var service = new ExceptionThrowingService();

            try
            {
                service.DoWork();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        [Test()]
        public void ThrowWithoutEx_StrackTraceIsAtLineOfOriginalException()
        {
            var service = new ExceptionThrowingService();

            try
            {
                service.DoWorkV2();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
