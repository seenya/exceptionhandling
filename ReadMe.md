# Exception Handling Examples

## Rethrowing Exceptions
When we catch exceptions there are many cases where we want to do a bit of processing, then pass the same exception up the chain of command. The obvious way to do this is to throw the exception you caught:

    try
    {
        MethodThatThrowsException();
    }
    catch (Exception ex)
    {
    	// Perform whatever logging/processing you desire.
        throw ex;
    }

Every time you do this, you actually **rewrite the Stack Trace!** This makes it significantly harder to resolve issues/defects/bugs/whatever you call 'em.

The correct way to rethrow an exception in C# is:

    try
    {
        MethodThatThrowsException();
    }
    catch (Exception ex)
    {
    	// Perform whatever logging/processing you desire.
        throw;
    }

Note that we use the `throw` keyword without specifying the Exception object that we want to throw. When we do this, the .Net compiler or runtime (I personally don't know which) throws the exception again and **keeps the Stack Trace intact**.

### Code Examples

Exceptions inflict a significant performance penalty whenever they are thrown. Exceptions should be reserved for exceptional circumstances. If 50% of the calls to a method result in an Exception being thrown, then this is not an exceptional situation, this is business as usual.

I appreciate that if the data is invalid, then from a theoretical point of view, it may be correct that an exception be thrown. However, if you want the services that you write to scale and deliver responsive user experiences, then consider the performance impact of all Exceptions that you throw and use them sparingly.

## Exception and Performance

Exceptions inflict a significant performance penalty whenever they are thrown. I would recommend that you treat exceptions for exceptional circumstances. If 50% of the calls to one of your methods result in an Exception being thrown, then this is not an _exceptional_ situation, this is _business as usual_.

I appreciate that if the data is invalid, then from a theoretical point of view, it is **correct** to many people that an exception be thrown. I have had a few discussions with people who get upset that I would even suggest otherwise. However, if you want the services that you write to scale and deliver responsive user experiences, I would recommend you consider the performance impact of all Exceptions that you throw.

### Code Examples

Run the ExceptionPerformanceService tests and view the console output to see how much of a performance penalty you pay for throwing Exceptions as a part of your normal processing.

### References
* [StackOverflow: What is the proper way to re-throw an exception in C#](https://stackoverflow.com/questions/178456/what-is-the-proper-way-to-re-throw-an-exception-in-c)
* [MSDN Magazine: Essential .NET - C# Exception Handling](https://msdn.microsoft.com/en-us/magazine/mt620018.aspx)
* [Microsoft Docs: CA2200: Rethrow to preserve stack details](https://docs.microsoft.com/en-us/visualstudio/code-quality/ca2200-rethrow-to-preserve-stack-details?view=vs-2017)
